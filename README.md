# Opdrag
Sagteware moet ontwikkel word om die bestuur van ‘n eiendoms agentskap te hanteer.  
Die sagteware bestaan uit drie afdelings: windows, web en selfoon. 
Al die sagteware moet in Visual Studio ontwikkel word met MS SQL Server Express of MySQL as databasis koppelvlak. 
Die web aspek moet geskryf word in ASP.Net met C# of VB.Net as server taal. 
Vir die web ontwikkeling mag ook gebruik gemaak word van Bootstrap CSS, JavaScript en JQuery. 
Selfoon sagteware moet in die Windows omgewing ontwikkel word (Xamarin is geskik vir die doel).

# Web
Hierdie gedeelte sluit in advertensies omtrent beskikbare eiendom en inligting omtrent eiendom wat verkoop/verhuur is gedurende sekere tydperke.

# Preview
The site is hosted at at http://realestate.stefanj.me

# Algemeen
Verdere funksionaliteit word nie spesifiseer nie en word aan die studente se inisiatief oorgelaat. 
Punte sal gegee word vir additionele funksionaliteit wat ‘n groot aantrekkingskrag in die mark sal hê om sodende teen potensiële kompetiesie te kan meeding.

# Deadline
Maandag 2 November 2015 

# Documentation
[Documentation is seperately maintained here](https://gitlab.com/stefan-j/422-Web-Dev-Documentation)

Online documentation can be found at http://documentation.realestate.stefanj.me

# Hilfe
* [How GIT works](https://git-scm.com/book/en/v2/Getting-Started-Git-Basics)
* [Forking, Push, Pull](http://scholarslab.org/research-and-development/forking-fetching-pushing-pulling/)
* [GUI APP - SourceTree](https://www.sourcetreeapp.com/)
* [Visual Studio Community](https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx)
* [ASP.NET Tutorials](http://www.w3schools.com/aspnet/)